#define LEDPIN 1

void setup()
{
   pinMode(LEDPIN,OUTPUT);
}

void loop()
{
   delay(200);
   digitalWrite(LEDPIN,HIGH);
     
   delay(200);
   digitalWrite(LEDPIN,LOW);

}
