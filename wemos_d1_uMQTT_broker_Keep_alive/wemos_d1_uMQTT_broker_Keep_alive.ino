/*
 * uMQTTBroker demo for Arduino
 * 
 * Minimal Demo: the program simply starts a broker and waits for any client to connect.
 */

#define BROKER 0
#include <ESP8266WiFi.h>
#if BROKER
#include "uMQTTBroker.h"
#endif
#include "MQTT.h"



#define CLIENT_ID "Broker_Client"
MQTT myMqtt(CLIENT_ID, "192.168.100.100", 1883);

#if BROKER
uMQTTBroker myBroker;
#endif

/*
 * Your WiFi config here
 */
char ssid[] = "Totalplay-2B9F";      // your network SSID (name)
char pass[] = "cemitas1024"; // your network password

//Static IP address configuration
IPAddress staticIP(192, 168, 100, 102); //ESP static ip
IPAddress gateway(192, 168, 100, 1);   //IP Address of your WiFi Router (Gateway)
IPAddress subnet(255, 255, 255, 0);  //Subnet mask
IPAddress dns(8, 8, 8, 8);  //DNS

const char* deviceName = "Test";


void myDataCb(String& topic, String& data);
void myPublishedCb();
void myDisconnectedCb();
void myConnectedCb();

int value = 0;
/*
 * WiFi init stuff
 */
void startWiFiClient()
{
  Serial.println("Connecting to "+(String)ssid);
  WiFi.hostname(deviceName);
  WiFi.config(staticIP, subnet, gateway, dns);
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
  WiFi.softAP(ssid, pass);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
}

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  

  // Connect to a WiFi network
  startWiFiClient();

  // Or start the ESP as AP
//startWiFiAP();
#if BROKER
  // Start the broker
  Serial.println("Starting MQTT broker");
  myBroker.init();
#endif
  myMqtt.onConnected(myConnectedCb);
  myMqtt.onDisconnected(myDisconnectedCb);
  myMqtt.onPublished(myPublishedCb);
  myMqtt.onData(myDataCb);
  
  Serial.println("connect mqtt...");
  myMqtt.connect();
  Serial.println("subscribe to topic...");
  myMqtt.subscribe("BrokerStatus");
}

void loop()
{   
  // do anything here
  delay(1000);
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  String topic("BrokerStatus");
#if 0
  topic += CLIENT_ID;
  topic += "/value";
#endif
  value++;
  String valueStr(value);
  
  boolean result = myMqtt.publish(topic, valueStr);
  Serial.println(value);
  delay(1000);
}

void myConnectedCb()
{
  Serial.println("connected to MQTT server");
}

void myDisconnectedCb()
{
  Serial.println("disconnected. try to reconnect...");
  delay(500);
  myMqtt.connect();
}

void myPublishedCb()
{
  //Serial.println("published.");
}

void myDataCb(String& topic, String& data)
{
  
  Serial.print(topic);
  Serial.print(": ");
  Serial.println(data);
}
