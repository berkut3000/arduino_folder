#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>
 
const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);
 
// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;
 
char data[32] = "Hola gatas";
char dato[32] = "Hola perros";
 
void setup(void)
{
	Serial.begin(9600);
   radio.begin();
   radio.openWritingPipe(pipe);
}
 
void loop(void)
{
	uint8_t status = ((~0xC3) & PIND);
	char status_str[20] = "";
	sprintf(status_str, "%u\r\n", status);
	Serial.println(status_str);
	radio.write(status_str, strlen(status_str));
	
#if 0
	switch(status)
	{
		case HIGH:
			radio.write(data, 32);
			break;
		case LOW:
			radio.write(dato,32);
			break;
#if 1
		default:
			radio.write("default",32);
			break;
#endif
	}
#endif
   delay(10);
}
