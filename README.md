# Arduino Folder

To avoid re-installing independent libraries, It was chosen to use this standalone folder to backup all the developed Arduino projects.

## Getting Started

Ensure to clone this Repo in

```
C:/Users/<username>/Documents/Arduino
```

or

```
~/Documents/Arduino
```

This action will substitute everything that was setup previously, so proceed with caution.


### Prerequisites

- [Arduino](https://www.arduino.cc/en/Main/Software)
- An Arduino Compatible Board


### Installing

Just install Arduino, and clone this repository into the specified folder.

## Running the tests

Open any other Sketch as normal. Compile and Upload to the required target. Don't forget to set the corresponding MCU and respective uploading port.


## Built With

* [Arduino](https://www.arduino.cc/en/Main/Software) - The Open Source Platform

## Extras

- There is a Website which serves as an introduction to the platform as well as some tutorials.
- The balancin robot project requires that the

```
arduino-self-balancing-robot-master
```
libraries be installed inside the libraries folder. However, there are uncompatibility issues when this folder is present, so its best to remove it whenever another sketch causes conflict during compilation.

## Versioning

GitLab is used for versioning.

## Authors

* **Antonio Aguilar Mota** - *Initial work* - [AntoMota](https://gitlab.com/berkut3000)


## License

This project is licensed ...

## Acknowledgments

* Arduino
