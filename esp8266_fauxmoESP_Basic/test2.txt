#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <fauxmoESP.h>
#include "credentials.h"
fauxmoESP fauxmo;
void setup() { 
  Serial.begin(115200);
  if (connectWifi()) {
    // Setup fauxmo
     Serial.println("Adding LED device");
     fauxmo.setPort(80);  
     fauxmo.enable(true);
     fauxmo.addDevice("Led");   
  }
}
void loop() {
  fauxmo.handle();
}
boolean connectWifi() {
  // Let us connect to WiFi
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(".......");
  Serial.println("WiFi Connected....IP Address:");
  Serial.println(WiFi.localIP());
  return true;
}