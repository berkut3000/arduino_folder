#include <SPI.h>
#include <LoRa.h>

#include <U8x8lib.h>

#ifdef ARDUINO_SAMD_MKRWAN1300
#error "This example is not compatible with the Arduino MKR WAN 1300 board!"
#endif

#define CONSOLE_DEBUG

// WIFI_LoRa_32 ports
// GPIO5  -- SX1278's SCK
// GPIO19 -- SX1278's MISO
// GPIO27 -- SX1278's MOSI
// GPIO18 -- SX1278's CS
// GPIO14 -- SX1278's RESET
// GPIO26 -- SX1278's IRQ(Interrupt Request)

#define SS      18
#define RST     14
#define DI0     26
#define BAND    902E6

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 15, /* data=*/ 4, /* reset=*/ 16);


String receivedText;
char cadena[20];
char currentid[64];

bool bandera = 0;
int iter = 0;
void setup() {
#ifdef CONSOLE_DEBUG
  Serial.begin(9600);
  while (!Serial);
#endif

  SPI.begin(5, 19, 27, 18);
  LoRa.setPins(SS, RST, DI0);
  
  

  u8x8.begin();
  u8x8.setFont(u8x8_font_chroma48medium8_r);
  //u8x8.setFlipDraw(1);
  
    u8x8.draw2x2String(3, 2, "SMART");
    u8x8.draw2x2String(5, 4, "GYM");
    delay(1000);
    //uint8_t tiles[16] = { 0x0f,0x0f,0x0f,0x0f,0xf0,0xf0,0xf0,0xf0, 1, 3, 7, 15, 31, 63, 127, 255};
    //u8x8.drawTile(1, 3, 2, tiles);
    delay(1000);

#ifdef CONSOLE_DEBUG  
  Serial.println("LoRa Receiver Callback");
#endif

  if (!LoRa.begin(BAND)) {
#ifdef CONSOLE_DEBUG
    Serial.println("Starting LoRa failed!");
#endif
    u8x8.drawString(0, 1, "Starting LoRa failed!");
    while (1);
  }

  // register the receive callback
  LoRa.onReceive(onReceive);

  // put the radio into receive mode
  LoRa.receive();
  u8x8.clear();
}

void loop() {
  // do nothing
    while(0 != bandera)
    {
        // memset(cadena,'\0',strlen(cadena));
    // }else{
        u8x8.setPowerSave(0);
        //u8x8.draw2x2String(0,3,"          ");
        u8x8.clear();
        u8x8.draw2x2String(0, 1, cadena);
        u8x8.draw2x2String(0, 3, cadena);
        u8x8.draw2x2String(0, 5, cadena);
        //strcpy(cadena,"");
        delay(500);
        // u8x8.setPowerSave(1);
        for (iter = 0; iter <6; iter ++)
        {
            u8x8.setPowerSave(1);
            delay(500);
            u8x8.setPowerSave(0);
            delay(500);
            // u8x8.setInverseFont(1);
            // u8x8.drawString(0, 0, "                 ");
            // u8x8.drawString(0, 1, "                 ");
            // u8x8.drawString(0, 2, "                 ");
            // u8x8.draw2x2String(0, 3, cadena);
            // u8x8.drawString(0, 5, "                 ");
            // u8x8.drawString(0, 6, "                 ");
            // u8x8.drawString(0, 7, "                 ");
            // delay(500);
            // u8x8.setInverseFont(0);
            // u8x8.drawString(0, 0, "                 ");
            // u8x8.drawString(0, 1, "                 ");
            // u8x8.drawString(0, 2, "                 ");
            // u8x8.draw2x2String(0, 3, cadena);
            // u8x8.drawString(0, 5, "                 ");
            // u8x8.drawString(0, 6, "                 ");
            // u8x8.drawString(0, 7, "                 ");
            // delay(500);
        }
            
        // u8x8.setInverseFont(1);
        // u8x8.drawString(0, 0, "                 ");
        // u8x8.drawString(0, 1, "                 ");
        // u8x8.drawString(0, 2, "                 ");
        // u8x8.draw2x2String(0, 3, cadena);
        // u8x8.drawString(0, 5, "                 ");
        // u8x8.drawString(0, 6, "                 ");
        // u8x8.drawString(0, 7, "                 ");
        // delay(500);
        // u8x8.setPowerSave(0);
        // u8x8.setInverseFont(0);
        // u8x8.draw1x2String(0, 0, "                 ");
        // u8x8.draw2x2String(0, 3, cadena);
        // delay(500);
        // u8x8.setPowerSave(1);
        // u8x8.setInverseFont(1);
        // u8x8.draw1x2String(0, 0, "                 ");
        // u8x8.draw2x2String(0, 3, cadena);
        // delay(500);
        // u8x8.setPowerSave(0);
        // u8x8.setInverseFont(0);
        // u8x8.draw1x2String(0, 0, "                 ");
        // u8x8.draw2x2String(0, 3, cadena);
        delay(2000);
        u8x8.clear();
        u8x8.setPowerSave(1);
        memset(cadena,'\0',strlen(cadena));
        bandera = 0;
    }
    
  
}

void onReceive(int packetSize) {
    
    bandera = 1;
  // received a packet
#ifdef CONSOLE_DEBUG
    //Serial.print("Received packet '");
#endif

  // read packet
    for (int i = 0; i < packetSize; i++) {
        receivedText = (char)LoRa.read();
#ifdef CONSOLE_DEBUG
        Serial.print(receivedText);
#endif
        
        receivedText.toCharArray(currentid, 64);
        strcat(cadena,currentid);

    }
    memset(currentid,'\0',64);
    bandera = 1;

  // print RSSI of packet
#ifdef CONSOLE_DEBUG
    // Serial.print("' with RSSI ");
    // Serial.println(LoRa.packetRssi());
#endif
    // u8x8.draw2x2String(0,3,"          ");
    // u8x8.draw2x2String(0, 3, cadena);
    // strcpy(cadena,"");
    // delay(500);
    // u8x8.setPowerSave(1);
    // delay(500);
    // u8x8.setPowerSave(0);
    // delay(500);
    // u8x8.setPowerSave(1);
    // delay(500);
    // u8x8.setPowerSave(0);
    // delay(500);
    // u8x8.clear();
    // u8x8.setPowerSave(1);
}

