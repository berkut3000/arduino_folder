/*
 * csc.h
 *
 *  Created on: 26/09/2018
 *      Author: hydro
 */

#ifndef CSC_H_
#define CSC_H_

/* Libraries to be included */
#include <stdint.h>

/* Function prototypes of the source file this header file references to */
uint8_t uiMy_strcmp(uint8_t *a, uint8_t *b);
uint8_t custom_strstr(uint8_t *a, uint8_t *b, uint8_t *expected_buffer_size, uint8_t *substring_size);


#endif /* CSC_H_ */