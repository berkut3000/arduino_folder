#include <nRF24L01.h>
#include <RF24.h>
#include <RF24_config.h>
#include <SPI.h>

extern "C"{
#include "csc.h"
}

#define BUZZER_PIN 8

const int pinCE = 9;
const int pinCSN = 10;
const uint8_t encendido_array[4] = "4\r\n";
RF24 radio(pinCE, pinCSN);
const int audio_output_pin = 0;
 
// Single radio pipe address for the 2 nodes to communicate.
const uint64_t pipe = 0xE8E8F0F0E1LL;
 
char data[32];
 
void setup(void)
{
   Serial.begin(9600);
   Serial.println("Iniciando Radio");
   bool radio_bool = false;
   radio.begin();
#if 0
   while (false == radio_bool)
   {
      radio_bool == radio.begin();
      
      Serial.println(radio_bool);
   }
#endif
   
   radio.openReadingPipe(1,pipe);
   radio.startListening();
   Serial.println("Radio Iniciado");
   
   pinMode(BUZZER_PIN, OUTPUT);
   digitalWrite(BUZZER_PIN, LOW);
}
 
void loop(void)
{
  
	while (radio.available())
	{
		radio.read(data, 32);
		Serial.print(data);
		
		uint8_t tam_data = strlen(data);
		uint8_t tam_subdata = strlen(encendido_array);
		if (0 == custom_strstr(data, encendido_array, &tam_data, &tam_subdata))
		{
			digitalWrite(BUZZER_PIN,HIGH);
		}else{
			digitalWrite(BUZZER_PIN,LOW);
		}
		
	}
}
