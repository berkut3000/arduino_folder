/*
 * @file csc.c
 * @brief Custom function for sring comparison
 *
 *  Created on: 26/09/2018
 *      Author: hydro
 */

/* Headers */

/* Required libraries for EFM32 Zero Gecko */
//#include "em_device.h"

/* String comparison Function */
#include "csc.h"

/* Functions */

/* Definitions */
#define SUCCESS     0
#define UNSUCCESS   1
#define UN_EVAL     2

/***************************************************************************//**
 * @brief
 *   Compares 2 strings.
 *
 * @details
 *   Compares 2 strings indicated by the mentioned pointers
 *
 * @note
 *   This was custom implemented, from an StackOVerflow function. Future
 *      revisions may or not include C's "strcmp() function.
 *
 * @param[in] uint8_t *a
 *   Species starting postion of first string to be compared.
 *
 * @param[in] uint8_t *b
 *   Species starting postion of second string to be compared.
 *
 * @return
 *   The decimal representation of the difference in characters between the 2
 *   strings, expected value to be 0.
 ******************************************************************************/
uint8_t uiMy_strcmp(uint8_t *a, uint8_t *b)
{
        while (*a && *b && *a == *b)
        {
                ++a;
                ++b;
        }
        return (unsigned char)(*a) - (unsigned char)(*b);
}

/***************************************************************************//**
 * @brief
 *   Find string inside string.
 *
 * @details
 *   Compares 2 strings and finds
 *
 * @note
 *   This was custom implemented, from an StackOVerflow function. Future
 *      revisions may or not include C's "strcmp() function.
 *
 * @param[in] uint8_t *a
 *   Species starting postion of first string to be compared.
 *
 * @param[in] uint8_t *b
 *   Species starting postion of second string to be compared.
 *
 * @return
 *   The decimal representation of the difference in characters between the 2
 *   strings, expected value to be 0.
 ******************************************************************************/
uint8_t custom_strstr(uint8_t *a, uint8_t *b, uint8_t *expected_buffer_size, uint8_t *substring_size)
{

	uint8_t cont = 0;
	uint8_t cont_incidencias = 0;
	uint8_t criterio_search = *expected_buffer_size - *substring_size;
	uint8_t iter = 0;

	if(*expected_buffer_size < *substring_size)
	{
		return UN_EVAL;
	}

	while(cont <= criterio_search)
	{
                cont_incidencias = 0;
		for(iter = 0; iter < *substring_size; iter++)
		{
			if( *(a+iter+cont) == *(b+iter) )
			{
				cont_incidencias++;
			}
		}
		if(cont_incidencias == *substring_size)
		{
			return SUCCESS;
		}
		cont++;
	}
    return UNSUCCESS;

}





