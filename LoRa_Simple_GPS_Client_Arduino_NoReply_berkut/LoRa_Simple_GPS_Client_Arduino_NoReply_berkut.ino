/*
In this project,we'll show how to get GPS data from a remote Arduino via Wireless Lora Protocol 
and show the track on the GoogleEarth.The construction of this project is similar to my last one:

1) Client Side: Arduino + Lora/GPS Shield (868Mhz).
2) Server Side: Arduino + Lora Shield (868Mhz) + Yun Shield + USB flash.

Client side will get GPS data and keep sending out to the server via Lora wireless. Server side 
will listin on the Lora wireless frequency, once it get the data from Client side, it will
turn on the LED and log the sensor data to a USB flash. 
Note: Over here we use the hardware serial to connect with the GPS and check the serial print by
the software serial.You should make sure the GPS get fixed(3D_Fix LED flash).
Press the "RST" button when you upload the sketch.
More about this example, please see:


*/
#include <SoftwareSerial.h>
#include <TinyGPS.h>
/* This sample code demonstrates the normal use of a TinyGPS object.
   It requires the use of SoftwareSerial, and assumes that you have a
   9600-baud serial GPS device hooked up on pins 3(rx) and 4(tx).
*/
#include <SPI.h>
#include <RH_RF95.h>

// Singleton instance of the radio driver
RH_RF95 rf95;
TinyGPS gps;
SoftwareSerial ss(4, 3);
String datastring="";
String datastring1="";
char databuf[100];
uint8_t dataoutgoing[100]="Hola";
char gps_lon[30]={"\0"};  
char gps_lat[30]={"\0"}; 
float frequency = 902.3;



void setup()
{
  Serial.begin(9600);
  ss.begin(9600);
    if (!rf95.init())
      Serial.println("init failed");

  Serial.println("Iniciando Sketch");
  // Setup ISM frequency
  rf95.setFrequency(frequency);
  // Setup Power,dBm
  rf95.setTxPower(13);

  // Setup Spreading Factor (6 ~ 12)
  rf95.setSpreadingFactor(7);
  
  // Setup BandWidth, option: 7800,10400,15600,20800,31250,41700,62500,125000,250000,500000
  //Lower BandWidth for longer distance.
  rf95.setSignalBandwidth(125000);
  
  // Setup Coding Rate:5(4/5),6(4/6),7(4/7),8(4/8) 
  rf95.setCodingRate4(5);
  
    // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
    Serial.print("Simple TinyGPS library v. "); Serial.println(TinyGPS::library_version());
    Serial.println();
}

void loop()
{ 
//  // Print Sending to rf95_server
//  Serial.println("Sending to rf95_server");
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;
//
//  float flat, flon;
//  unsigned long age, date, time, chars = 0;
//
//  static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;
//
//  gps.f_get_position(&flat, &flon, &age);
//  print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
//  print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);
//  Serial.println();

//  smartdelay(1000);
//
  // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;)
  {
    while (ss.available())
    {
      char c = ss.read();
//      Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)){// Did a new valid sentence come in?
                newData = true;
      }
    }
  }
//  //Get the GPS data
  if (newData)
  {
    float flat, flon;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
//    print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
//    print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);
    nl();
//    Serial.print("LAT=");
//    Serial.print(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6);
//    Serial.print(" LON=");
//    Serial.print(flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6);
//    Serial.print(" SAT=");
//    Serial.print(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
//    Serial.print(" PREC=");
//    Serial.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
//    flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6;          
//    flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6; 
//    // Once the GPS fixed,send the data to the server.
    datastring +=dtostrf(flat, 0, 6, gps_lat); 
    datastring1 +=dtostrf(flon, 0, 6, gps_lon);
//    Serial.println(datastring);
//    Serial.println(datastring1);
    
    
//    String coordinates =datastring+","+datastring1;
//    Serial.println(coordinates);

//    Serial.println(strcat(strcat(gps_lon,","),gps_lat));
    strcat(gps_lat,",");
    strcat(gps_lat,gps_lon);
//    Serial.println(gps_lat); //Print gps_lon and gps_lat
    strcpy((char *)dataoutgoing,gps_lat); 
    // Send the data to server
    
    Serial.println("Preparando para enviar");
    rf95.send(dataoutgoing, sizeof(dataoutgoing));

    rf95.waitPacketSent();
//    Serial.println(coordinates);
    datastring = "";
    datastring1 = "";
//    coordinates = "";
    
    

    
    
    



    
//   
//    // Now wait for a reply
//    uint8_t indatabuf[RH_RF95_MAX_MESSAGE_LEN];
//    uint8_t len = sizeof(indatabuf);
//
//    if (rf95.waitAvailableTimeout(3000))
//     { 
//       // Should be a reply message for us now   
//       if (rf95.recv(indatabuf, &len))
//      {
//         // Serial print "got reply:" and the reply message from the server
//         ss.print("got reply: ");
//         ss.println((char*)indatabuf);
//      }
//      else
//      {
//      ss.println("recv failed");
//      }
//    }
//    else
//    {
//      // Serial print "No reply, is rf95_server running?" if don't get the reply .
//      ss.println("No reply, is rf95_server running?");
//    }
//  delay(400);
//    
 }
  
  gps.stats(&chars, &sentences, &failed);                                                                                                                                                                                                                                                                                                                                                                          
  Serial.print(" CHARS=");
  Serial.print(chars);
  Serial.print(" SENTENCES=");
  Serial.print(sentences);
  Serial.print(" CSUM ERR=");
  Serial.println(failed);
  if (chars == 0)
  Serial.println("** No characters received from GPS: check wiring **");
  nl();
}


















void nl(void){
  Serial.println();
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

static void print_float(float val, float invalid, int len, int prec)
{
  if (val == invalid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i=flen; i<len; ++i)
      Serial.print(' ');
  }
  smartdelay(0);
}

static void print_int(unsigned long val, unsigned long invalid, int len)
{
  char sz[32];
  if (val == invalid)
    strcpy(sz, "*******");
  else
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i=strlen(sz); i<len; ++i)
    sz[i] = ' ';
  if (len > 0) 
    sz[len-1] = ' ';
  Serial.print(sz);
  smartdelay(0);
}

static void print_date(TinyGPS &gps)
{
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned long age;
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  if (age == TinyGPS::GPS_INVALID_AGE)
    Serial.print("********** ******** ");
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d %02d:%02d:%02d ",
        month, day, year, hour, minute, second);
    Serial.print(sz);
  }
  print_int(age, TinyGPS::GPS_INVALID_AGE, 5);
  smartdelay(0);
}

static void print_str(const char *str, int len)
{
  int slen = strlen(str);
  for (int i=0; i<len; ++i)
    Serial.print(i<slen ? str[i] : ' ');
  smartdelay(0);
}
