/*
 * atmega328p_atmel_studio_blink.c
 *
 * Created: 11/03/2019 10:17:39 p. m.
 * Author : AntoMota
 */

#define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	DDRB |= 0x20;
    /* Replace with your application code */
    while (1) 
    {
		PORTB = 0x20;
		_delay_ms(2000);
		_delay_ms(2000);
		
		PORTB = 0x00;
		_delay_ms(2000);
		_delay_ms(2000);
    }
}

