/******************************************************************************
 * @ file template.c
 *
 *  Created on: DD/MM/2019
 *  Modified on: DD/MM/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <Wire.h>

/* New data types */

/* Constant defintions */

/* Macro definitions */
#define DEBUG 1

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/
void setup()
{
    
    Wire.begin(); //join i2c bus
    SerialUSB.begin(9600);
    
#if 0
  SerialUSB.begin(115200);
  while (!SerialUSB) ;
  SerialUSB.println("\nI2C Scanner");
#endif
}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields
 ******************************************************************************/
void loop()
{
    int address = 0;
    int error  = 0;
    for(address = 59; address < 61; address++ )//Start at known valid address
    {
        bool flag = 1;

        SerialUSB.print("checking address ");
        SerialUSB.println(address);

        Wire.requestFrom(address,10);//guessing 10 bytes is enough
        SerialUSB.println("debug1");

        while(Wire.available())
        {
            SerialUSB.println("debug2");
            int b = Wire.read();
            SerialUSB.println(b, HEX);
            flag = 0;
        }

        if(flag)//wire not available
        {
            SerialUSB.println("nothing at this address");
        }
        SerialUSB.println("debug3");
        error = Wire.endTransmission();
        delay(700);
    }


#if 0
    String  aux = "scanner";
    SerialUSB.println(aux);
    uint8_t error       = 0;
    uint8_t address     = 0;
    uint8_t nDevices    = 0;
    int     buffer_read = 0;
    
    for(address = 1; address < 127; address++ ) 
    {
        // The i2c_scanner uses the return value of
        // the Write.endTransmisstion to see if
        // a device did acknowledge to the address.
        Wire.requestFrom(address,10);
        #if DEBUG
        SerialUSB.println("Iniciando Transmision I2C");
        #endif
        int b = Wire.read();
        error = Wire.endTransmission();
        #if DEBUG
        SerialUSB.println("Finalizando Transmision I2C");
        #endif
        if (error == 0)
        {
          SerialUSB.print("I2C device found at address 0x");
          if (address<16) 
            SerialUSB.print("0");
          SerialUSB.print(address,HEX);
          SerialUSB.println("  !");

          nDevices++;
        }
        else if (error==4) 
        {
          SerialUSB.print("Unknown error at address 0x");
          if (address<16) 
            SerialUSB.print("0");
          SerialUSB.println(address,HEX);
        }    
      }
      if (nDevices == 0)
        SerialUSB.println("No I2C devices found\n");
      else
        SerialUSB.println("done\n");
#endif

}

/* Private Functions */

/*** end of file ***/
